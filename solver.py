import os, sys
import random
from colorama import init, Fore, Back, Style
init()


def get_wordlist(loc):
    wordlist  = []
    with open(loc, "r") as wlfp:
        wordlist = [x.strip() for x in wlfp.readlines()]
    print(f"Got wordlist with {len(wordlist)} words.")
    return wordlist
        

def get_seedlist(wordlist):
    seedlist = []
    for word in wordlist:
        if len(set(word)) == 5:
            vowels = 0
            for letter in word:
                if letter in "aeiouy":
                    vowels += 1
            if vowels >= 2:
                commons = 0
                for letter in word:
                    if letter in "tslnrp":
                        commons += 1
                if commons + vowels > 4:
                    seedlist.append(word)
    print(f"Got seedlist with {len(seedlist)} words.")
    return seedlist
    
def get_solution(wordlist):
    solution = {"tl": "",
                "tr": "",
                "bl": "",
                "br": ""}
    if len(sys.argv) >= 5:
        if len(sys.argv[1]) == 5 and sys.argv[1] in wordlist:
            solution["tl"] = sys.argv[1]
        else:
            print(f"ERROR: Invalid word '{sys.argv[1]}'")
            exit()
        if len(sys.argv[2]) == 5 and sys.argv[2] in wordlist:
            solution["tr"] = sys.argv[2]
        else:
            print(f"ERROR: Invalid word '{sys.argv[2]}'")
            exit()
        if len(sys.argv[3]) == 5 and sys.argv[3] in wordlist:
            solution["bl"] = sys.argv[3]
        else:
            print(f"ERROR: Invalid word '{sys.argv[3]}'")
            exit()
        if len(sys.argv[4]) == 5 and sys.argv[4] in wordlist:
            solution["br"] = sys.argv[4]
        else:
            print(f"ERROR: Invalid word '{sys.argv[4]}'")
            exit()
    else:
        sollist = []
        while len(sollist) < 4:
            index = random.randrange(0,len(wordlist))
            if wordlist[index] not in sollist:
                sollist.append(wordlist[index])
        solution["tl"] = sollist[0]
        solution["tr"] = sollist[1]
        solution["bl"] = sollist[2]
        solution["br"] = sollist[3]   
    return solution
  
def determine_guess(guess_count, seedlist, guesslist, words):
    #First two guesses randomly pulled from seedlist. Next 7 guesses randomly pulled from wordlist
    print("-----------")
    if guess_count == 0:
        #Performance issues keep us from using this
        # impactDict = {}
        # for seed in seedlist:
            # impactDict[get_guess_impact(seed, words['all'])] = seed
            # topImpact = max(impactDict.keys())
            # guess = impactDict[topImpact]
        index = random.randrange(0,len(seedlist))
        guess = seedlist[index]
        
    elif guess_count == 1:
        guess = ""
        activeseedlist = []
        for seed in seedlist:
            if seed in words['all']:
                activeseedlist.append(seed)
        filteredseedlist = []
        vowelfilteredseedlist = []
        for seed in activeseedlist:
            keep = True
            for letter in guesslist[0]:
                if letter in "aeiouy" and letter in seed:
                    keep = False
                    break
            if keep:
                vowelfilteredseedlist.append(seed)
            keep = True
            for letter in guesslist[0]:
                if letter in seed:
                    keep = False
                    break
            if keep:
                filteredseedlist.append(seed)
        print(f"Seedlist has {len(filteredseedlist)} non-duplicate words and {len(vowelfilteredseedlist)} vowel non-duplicate words.") 
        if len(filteredseedlist):
            while guess == "" or guess in guesslist:
                mincount = 6
                minloc = "seedlist"
                for loc, wordlist in words.items():                  
                    if len(wordlist) < mincount and len(wordlist) > 0:
                        mincount = len(wordlist)
                        minloc = loc
                        #highest impact (most words to be eliminated from the quadrant's wordlist)
                        impactDict = {}
                        for seed in wordlist:
                            impactDict[get_guess_impact(seed, wordlist)] = seed
                        if len(wordlist) > 10:
                            topImpactList = []
                            while len(topImpactList) < 10 and len(topImpactList) < len(impactDict.keys())/2:
                                topImpactIdx = max(impactDict.keys())
                                topImpactList.append(impactDict[topImpactIdx])
                                del impactDict[topImpactIdx]
                            index = random.randrange(0,len(topImpactList))
                            guess = topImpactList[index]                        
                        else:
                            topImpact = max(impactDict.keys())
                            guess = impactDict[topImpact]
                        #random
                        #index = random.randrange(0,len(wordlist))
                        #guess = wordlist[index]
                if guess == "":
                    #highest impact (most words to be eliminated from the overall wordlist)
                    impactDict = {}
                    for seed in filteredseedlist:
                        impactDict[get_guess_impact(seed, words['all'])] = seed
                    if len(wordlist) > 10:
                        topImpactList = []
                        while len(topImpactList) < 10 and len(topImpactList) < len(impactDict.keys())/2:
                            topImpactIdx = max(impactDict.keys())
                            topImpactList.append(impactDict[topImpactIdx])
                            del impactDict[topImpactIdx]
                        index = random.randrange(0,len(topImpactList))
                        guess = topImpactList[index]                        
                    else:
                        topImpact = max(impactDict.keys())
                        guess = impactDict[topImpact]
                    #index = random.randrange(0,len(filteredseedlist))
                    #guess = filteredseedlist[index]
            print(f"Guess '{guess}' chosen from {minloc} wordlist")
        elif len(vowelfilteredseedlist):
            while guess == "" or guess in guesslist:
                mincount = 11
                minloc = "seedlist"
                for loc, wordlist in words.items():                  
                    if len(wordlist) < mincount and len(wordlist) > 0:
                        mincount = len(wordlist)
                        minloc = loc
                        #highest impact (most words to be eliminated from the quadrant's wordlist)
                        impactDict = {}
                        for seed in wordlist:
                            impactDict[get_guess_impact(seed, wordlist)] = seed
                        if len(wordlist) > 10:
                            topImpactList = []
                            while len(topImpactList) < 10 and len(topImpactList) < len(impactDict.keys())/2:
                                topImpactIdx = max(impactDict.keys())
                                topImpactList.append(impactDict[topImpactIdx])
                                del impactDict[topImpactIdx]
                            index = random.randrange(0,len(topImpactList))
                            guess = topImpactList[index]                        
                        else:
                            topImpact = max(impactDict.keys())
                            guess = impactDict[topImpact]
                        #random
                        #index = random.randrange(0,len(wordlist))
                        #guess = wordlist[index]
                if guess == "":
                    #highest impact (most words to be eliminated from the overall wordlist)
                    impactDict = {}
                    for seed in vowelfilteredseedlist:
                        impactDict[get_guess_impact(seed, words['all'])] = seed
                    if len(wordlist) > 10:
                        topImpactList = []
                        while len(topImpactList) < 10 and len(topImpactList) < len(impactDict.keys())/2:
                            topImpactIdx = max(impactDict.keys())
                            topImpactList.append(impactDict[topImpactIdx])
                            del impactDict[topImpactIdx]
                        index = random.randrange(0,len(topImpactList))
                        guess = topImpactList[index]                        
                    else:
                        topImpact = max(impactDict.keys())
                        guess = impactDict[topImpact]
                    #random
                    #index = random.randrange(0,len(vowelfilteredseedlist))
                    #guess = vowelfilteredseedlist[index]
            print(f"Guess '{guess}' chosen from {minloc} wordlist")
        else:
            while guess == "" or guess in guesslist:
                mincount = 25
                minloc = "all"
                for loc, wordlist in words.items():
                    if len(wordlist) < mincount and len(wordlist) > 0:
                        mincount = len(wordlist)
                        minloc = loc
                        #highest impact (most words to be eliminated from the quadrant's wordlist)
                        impactDict = {}
                        for seed in wordlist:
                            impactDict[get_guess_impact(seed, wordlist)] = seed
                        topImpactList = []
                        while len(topImpactList) < 10 and len(impactDict):
                            topImpactIdx = max(impactDict.keys())
                            topImpactList.append(impactDict[topImpactIdx])
                            del impactDict[topImpactIdx]
                        index = random.randrange(0,len(topImpactList))
                        guess = topImpactList[index]                        
                        #topImpact = max(impactDict.keys())
                        #guess = impactDict[topImpact]
                        #random
                        #index = random.randrange(0,len(wordlist))
                        #guess = wordlist[index]
                if guess == "":
                    while guess == "" or len(set(guess)) < 5:
                        vowelfilteredwordlist = []
                        filteredwordlist = []
                        for word in words["all"]:
                            keep = True
                            for letter in guesslist[0]:
                                if letter in "aeiouy" and letter in word:
                                    keep = False
                                    break
                            if keep:
                                vowelfilteredwordlist.append(word)
                            keep = True
                            for letter in guesslist[0]:
                                if letter in word:
                                    keep = False
                                    break
                            if keep:
                                filteredwordlist.append(word)
                        if len(filteredwordlist):
                            minloc = "filtered"
                            #highest impact (most words to be eliminated from the entire wordlist)
                            impactDict = {}
                            for word in filteredwordlist:
                                impactDict[get_guess_impact(word, words['all'])] = word
                            if len(wordlist) > 10:
                                topImpactList = []
                                while len(topImpactList) < 10 and len(topImpactList) < len(wordlist)/2:
                                    topImpactIdx = max(impactDict.keys())
                                    topImpactList.append(impactDict[topImpactIdx])
                                    del impactDict[topImpactIdx]
                                index = random.randrange(0,len(topImpactList))
                                guess = topImpactList[index]                        
                            else:
                                topImpact = max(impactDict.keys())
                                guess = impactDict[topImpact]
                            #random
                            #index = random.randrange(0,len(filteredwordlist))
                            #guess = filteredwordlist[index]
                        elif len(vowelfilteredwordlist):
                            minloc = "vowelfiltered"
                            #highest impact (most words to be eliminated from the entire wordlist)
                            impactDict = {}
                            for word in vowelfilteredwordlist:
                                impactDict[get_guess_impact(word, words['all'])] = word
                            if len(wordlist) > 10:
                                topImpactList = []
                                while len(topImpactList) < 10 and len(topImpactList) < len(impactDict.keys())/2:
                                    topImpactIdx = max(impactDict.keys())
                                    topImpactList.append(impactDict[topImpactIdx])
                                    del impactDict[topImpactIdx]
                                index = random.randrange(0,len(topImpactList))
                                guess = topImpactList[index]                        
                            else:
                                topImpact = max(impactDict.keys())
                                guess = impactDict[topImpact]
                            #random
                            #index = random.randrange(0,len(vowelfilteredwordlist))
                            #guess = vowelfilteredwordlist[index]
                        else:
                            #highest impact (most words to be eliminated from the entire wordlist)
                            impactDict = {}
                            for word in words['all']:
                                impactDict[get_guess_impact(word, words['all'])] = word
                            if len(wordlist) > 10:
                                topImpactList = []
                                while len(topImpactList) < 10 and len(topImpactList) < len(impactDict.keys())/2:
                                    topImpactIdx = max(impactDict.keys())
                                    topImpactList.append(impactDict[topImpactIdx])
                                    del impactDict[topImpactIdx]
                                index = random.randrange(0,len(topImpactList))
                                guess = topImpactList[index]                        
                            else:
                                topImpact = max(impactDict.keys())
                                guess = impactDict[topImpact]
                            #random
                            #index = random.randrange(0,len(words["all"]))
                            #guess = words["all"][index]
                    print(f"Wordlist has {len(filteredwordlist)} non-duplicate words and {len(vowelfilteredwordlist)} vowel non-duplicate words.")
            print(f"Guess '{guess}' chosen from {minloc} wordlist")
    else:
        guess = ""
        while guess == "" or guess in guesslist:
            mincount = 40
            minloc = "all"
            for loc, wordlist in words.items():
                if len(wordlist) < mincount and len(wordlist) > 0:
                    mincount = len(wordlist)
                    minloc = loc
                    #highest impact (most words to be eliminated from the quadrant's wordlist)
                    impactDict = {}
                    for word in wordlist:
                        impactDict[get_guess_impact(word, wordlist)] = word
                    if len(wordlist) > 10:
                        topImpactList = []
                        #print(impactDict.keys())
                        while len(topImpactList) < 10 and len(topImpactList) < len(impactDict.keys())/2:
                            topImpactIdx = max(impactDict.keys())
                            topImpactList.append(impactDict[topImpactIdx])
                            del impactDict[topImpactIdx]
                        index = random.randrange(0,len(topImpactList))
                        guess = topImpactList[index]                        
                    else:
                        topImpact = max(impactDict.keys())
                        guess = impactDict[topImpact]
                    #random
                    #index = random.randrange(0,len(wordlist))
                    #guess = wordlist[index]            
            if guess == "":
                #highest impact (most words to be eliminated from the overall wordlist)
                impactDict = {}
                for word in words['all']:
                    impactDict[get_guess_impact(word, words['all'])] = word
                if len(wordlist) > 10:
                    topImpactList = []
                    while len(topImpactList) < 10 and len(topImpactList) < len(impactDict.keys())/2:
                        topImpactIdx = max(impactDict.keys())
                        topImpactList.append(impactDict[topImpactIdx])
                        del impactDict[topImpactIdx]
                    index = random.randrange(0,len(topImpactList))
                    guess = topImpactList[index]                        
                else:
                    topImpact = max(impactDict.keys())
                    guess = impactDict[topImpact]
                #random
                #index = random.randrange(0,len(words["all"]))
                #guess = words['all'][index]
        print(f"Guess '{guess}' chosen from {minloc} wordlist")
    
    print(f"Guess {guess_count + 1}: {guess}")
    return guess
    
def make_guess(guess, solution, results):
    for loc, word in solution.items():
        result = [0,0,0,0,0]
        letterIdx = 0
        for letter in guess:
            if letter == word[letterIdx]:
                result[letterIdx] = 2
            elif letter in word:
                result[letterIdx] = 1
            letterIdx += 1
        results[loc].append(result)
   
def print_result(guess_list, result):
    guessIdx = 0
    resultText = ""
    top = ""
    bottom = ""  
    for guess in guess_list:       
        letterIdx = 0
        topLeft = ""
        topRight = ""
        bottomLeft = ""
        bottomRight = ""
        for letter in guess:
            if result["tl"][guessIdx][letterIdx] == 2:
                topLeft += Back.GREEN + Fore.BLACK + letter
            elif result["tl"][guessIdx][letterIdx] == 1:
                topLeft += Back.YELLOW + Fore.BLACK + letter
            else:
                topLeft += Back.WHITE + Fore.BLACK + letter
                
            if result["tr"][guessIdx][letterIdx] == 2:
                topRight += Back.GREEN + Fore.BLACK + letter
            elif result["tr"][guessIdx][letterIdx] == 1:
                topRight += Back.YELLOW + Fore.BLACK + letter
            else:
                topRight += Back.WHITE + Fore.BLACK + letter
                
            if result["bl"][guessIdx][letterIdx] == 2:
                bottomLeft += Back.GREEN + Fore.BLACK + letter
            elif result["bl"][guessIdx][letterIdx] == 1:
                bottomLeft += Back.YELLOW + Fore.BLACK + letter
            else:
                bottomLeft += Back.WHITE + Fore.BLACK + letter
                
            if result["br"][guessIdx][letterIdx] == 2:
                bottomRight += Back.GREEN + Fore.BLACK + letter
            elif result["br"][guessIdx][letterIdx] == 1:
                bottomRight += Back.YELLOW + Fore.BLACK + letter
            else:
                bottomRight += Back.WHITE + Fore.BLACK + letter
            letterIdx += 1
        topLeft += Style.RESET_ALL 
        topRight += Style.RESET_ALL
        top += f"{topLeft}    {topRight}\n"
        bottomLeft += Style.RESET_ALL
        bottomRight += Style.RESET_ALL
        bottom += f"{bottomLeft}    {bottomRight}\n"
        guessIdx += 1
    resultText += f"{top}\n\n{bottom}"
    print(resultText)

def filter_words(guess, results, words):
    available_words = []
    for loc, res in results.items():
        length = len(res)
        result = res[length - 1]      
        letterIdx = 0
        wordlist = words[loc]
        filteredlist = []
        for letter in guess:
            if result[letterIdx] == 2:
                for word in wordlist:
                    if word[letterIdx] == letter and word != guess:
                        filteredlist.append(word)
            elif result[letterIdx] == 1:
                for word in wordlist:
                    if word[letterIdx] != letter and letter in word:
                        filteredlist.append(word)
            else:
                for word in wordlist:
                    if letter not in word:
                        filteredlist.append(word)
            wordlist = filteredlist
            filteredlist = []
            letterIdx += 1
        print(f"Filtered wordlist for {loc}. Removed {len(words[loc]) - len(wordlist)} words. {len(wordlist)} words left.")
        words[loc] = wordlist
        available_words = list(set(available_words + wordlist))
    print(f"Total wordlist has {len(available_words)} words left.")
    words["all"] = available_words
    
def update_scores(scores, guess, solution, guess_count):
    for loc, word in solution.items():
        if guess == word:
            scores[loc] = guess_count
            
            
def format_scores(scores):
    for loc,score in scores.items():
        if score == 0:
            scores[loc] = Back.RED + " " + Style.RESET_ALL
        else:
            scores[loc] = Back.BLUE + f"{score}" + Style.RESET_ALL
            
def get_guess_impact(guess, wordlist):
    impact = 0
    for i in range(3):
        for j in range(3):
            for k in range(3):
                for l in range(3):
                    for m in range(3):
                        results = [i,j,k,l,m]
                        index = 0
                        tempwordlist = wordlist
                        filteredlist = []
                        for letter in guess:
                            if results[index] == 2:
                                for word in tempwordlist:
                                    if word[index] == letter and word != guess:
                                        filteredlist.append(word)
                            elif results[index] == 1:
                                for word in wordlist:
                                    if word[index] != letter and letter in word:
                                        filteredlist.append(word)
                            else:
                                for word in wordlist:
                                    if letter not in word:
                                        filteredlist.append(word)
                            tempwordlist = filteredlist
                            filteredlist = []
                            index += 1
                        impact += len(wordlist) - len(tempwordlist)
    return impact


wl = get_wordlist("C:/Users/alex/Desktop/5Lwords.txt")
sl = get_seedlist(wl)
solution = get_solution(wl)
guess_count = 0
word_matrix = {"tl": wl,
               "tr": wl,
               "bl": wl,
               "br": wl,
               "all": wl}
guess_list = []
result_matrix = {"tl": [],
                 "tr": [],
                 "bl": [],
                 "br": []}
score_matrix = {"tl": 0,
                "tr": 0,
                "bl": 0,
                "br": 0}
solved = False
while guess_count < 9 and not solved:
    guess = determine_guess(guess_count, sl, guess_list, word_matrix)
    guess_list.append(guess)
    guess_count += 1
    make_guess(guess, solution, result_matrix)
    #print_result(guess_list, result_matrix)
    filter_words(guess, result_matrix, word_matrix)
    update_scores(score_matrix, guess, solution, guess_count)
    solved = True
    for loc, score in score_matrix.items():
        if score == 0:
            solved = False
            break
format_scores(score_matrix)
print_result(guess_list, result_matrix)
print(f"{score_matrix['tl']} {score_matrix['tr']}\n\n{score_matrix['bl']} {score_matrix['br']}")
print(f"{solution['tl']} {solution['tr']}\n{solution['bl']} {solution['br']}")
            

